#!/bin/bash

#Checking file exists
FILE=/usr/bin/ffmpeg
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else
    sudo apt update && sudo apt install ffmpeg
fi

echo "Исходное видео:" & read sourceVideo
  while true
  do
    echo "Начало записи:"
    read start
    echo "Конец записи:"
    read stop
    echo "Название нового видео:"
    read newVideo

    date1=$(date -u -d $start +%s)
    date2=$(date -u -d $stop +%s)
    diff=$(($date2-$date1))
    echo "$(date -u -d @$diff +"%H:%M:%S")"
    cont=$(date -u -d @$diff +"%H:%M:%S")

    ffmpeg -ss $start -t $cont -i $sourceVideo -qscale 0 $newVideo
  done
